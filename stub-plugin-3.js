function StubPlugin3(emit) {
  this.emit = emit;
}

StubPlugin3.prototype.hasEmit = function () {
  return Promise.resolve(!!this.emit);
};

StubPlugin3.prototype.emitHello = function () {
  this.emit('hello', { message: 'there'});
  
  return Promise.resolve();
};

module.exports = StubPlugin3;
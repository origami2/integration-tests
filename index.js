var o2 = require('origami2');
var StackIO = require('origami2-stack-io');
var PluginIO = require('origami2-plugin-io');
var ClientIO = require('origami2-client-io');
var PubSubIO = require('origami2-pubsub-io');
var TokenProviderClientIO = require('origami2-token-provider-client-io');
var TokenProviderIO = require('origami2-token-provider-io');
var data = require('./data');
var assert = require('assert');
var StubPlugin1 = require('./stub-plugin-1');
var StubPlugin2 = require('./stub-plugin-2');
var StubPlugin3 = require('./stub-plugin-3');
var StubPlugin4 = require('./stub-plugin-4');
var StubTokenProvider1 = require('./stub-token-provider-1');
var EmittersBus = o2.EmittersBus;
var EventEmitter = require('events').EventEmitter;

describe('Origami2 integration', function () {
  var stackIo;
  
  it('initializes stack', function () {
    stackIo = new StackIO(data.stack.privateKey);
    
    assert(stackIo);
  });
  
  var stack;
  
  it('gets stack', function () {
    stack = stackIo.getStack();
    
    assert(stack);
  });
  
  var plugin1io;
  
  it('initializes plugin1', function () {
    plugin1io = new PluginIO(
      StubPlugin1, 
      data.plugin1.privateKey, 
      {}, 
      {}
    );
    
    assert(plugin1io);
  });
  
  
  it('authorizes plugin1', function () {
    stackIo.authorizePlugin('StubPlugin1', data.plugin1.publicKey);
  });
  
  it('connectes plugin1', function (done) {
    var bus = new EmittersBus();
    
    stackIo
    .listenSocket(bus.createEmitter());
    plugin1io
    .listenSocket(bus.createEmitter())
    .then(function () {
      done();
    });
  });
  
  it('plugin1 is listed', function () {
    var apis = stack.listPlugins();
    
    assert(apis.StubPlugin1);
  });
  
  var tokenProvider1io;
  
  it('initializes tokenProvider1', function () {
    tokenProvider1io = new TokenProviderIO(
      StubTokenProvider1,
      data.tokenProvider1.privateKey,
      {},
      {}
    );
    
    assert(tokenProvider1io);
  });
  
  it('authorizes tokenProvider1', function () {
    stackIo.authorizeTokenProvider('StubTokenProvider1', data.tokenProvider1.publicKey);
  });
  
  
  it('connectes tokenProvider1', function (done) {
    var bus = new EmittersBus();
    
    stackIo
    .listenSocket(bus.createEmitter());
    
    tokenProvider1io
    .listenSocket(bus.createEmitter())
    .then(function () {
      done();
    });
  });
  
  it('tokenProvider1 is listed', function () {
    var apis = stack.listTokenProviders();
    
    assert(apis.StubTokenProvider1);
  });
  
  var tokenProviderClient1io;
  
  it('initializes tokenProviderClient1', function () {
    tokenProviderClient1io = new TokenProviderClientIO(
      data.client1.privateKey
    );
    
    assert(tokenProviderClient1io);
  });
  
  var tokenProviderClient1;
  
  it('tokenProviderClient1 connects', function (done) {
    var bus = new EmittersBus();
    
    stackIo
    .listenSocket(bus.createEmitter());
    
    tokenProviderClient1io
    .listenSocket(bus.createEmitter())
    .then(function (client) {
      tokenProviderClient1 = client;
      
      try {
        assert(tokenProviderClient1);
        assert(tokenProviderClient1.StubTokenProvider1);
        assert(tokenProviderClient1.StubTokenProvider1.getUserToken);
        
        done();
      } catch (e) {
        done(e);
      }
    });
  });
  
  var token1;
  
  it('tokenProviderClient1 gets token', function (done) {
    tokenProviderClient1
    .StubTokenProvider1
    .getUserToken('user1', 'letmepass')
    .then(function (token) {
      try {
        token1 = token;
        assert(token);
        
        done();
      } catch (e) {
        done(e);
      }
    });
  });
  
  it('token1 is valid', function () {
    var context = stack.getContextFromToken(token1);
    
    assert(context);
    assert.equal('user1', context.username);
  });
  
  var client1io;
  
  it('initializes client1', function () {
    client1io = new ClientIO(data.client1.privateKey);
    
    assert(client1io);
  });
  
  var client1;
  
  it('client1 connects', function (done) {
    var bus = new EmittersBus();
    
    stackIo
    .listenSocket(bus.createEmitter());
    
    client1io
    .listenSocket(bus.createEmitter())
    .then(function (client) {
      client1 = client;
      
      try {
        assert(client1);
        assert(client1.StubPlugin1);
        assert(client1.StubPlugin1.echo);
        
        done();
      } catch (e) {
        done(e);
      }
    });
  });
  
  it('client1 StubPlugin1.echo method works', function (done) {
    client1
    .StubPlugin1
    .echo('hello')
    .then(function (echoed) {
      try {
        assert.equal('hello', echoed);
        
        done();
      } catch (e) {
        done(e);
      }
    });
  });
  
  var plugin2io;
  
  it('initializes plugin2', function () {
    plugin2io = new PluginIO(
      StubPlugin2, 
      data.plugin2.privateKey, 
      {}, 
      {}
    );
    
    assert(plugin2io);
  });
  
  
  it('authorizes plugin2', function () {
    stackIo.authorizePlugin('StubPlugin2', data.plugin2.publicKey);
  });
  
  it('connectes plugin2', function (done) {
    var bus = new EmittersBus();
    
    stackIo
    .listenSocket(bus.createEmitter());
    plugin2io
    .listenSocket(bus.createEmitter())
    .then(function () {
      done();
    });
  });
  
  it('plugin2 is listed', function () {
    var apis = stack.listPlugins();
    
    assert(apis.StubPlugin2);
  });
  
  var plugin3io;
  
  it('initializes plugin3', function () {
    plugin3io = new PluginIO(
      StubPlugin3, 
      data.plugin3.privateKey, 
      {}, 
      {}
    );
    
    assert(plugin3io);
  });
  
  it('authorizes plugin3', function () {
    stackIo.authorizePlugin('StubPlugin3', data.plugin3.publicKey);
  });
  
  it('connectes plugin3', function (done) {
    var bus = new EmittersBus();
    
    stackIo
    .listenSocket(bus.createEmitter());
    plugin3io
    .listenSocket(bus.createEmitter())
    .then(function () {
      done();
    });
  });
  
  it('plugin3 is listed', function () {
    var apis = stack.listPlugins();
    
    assert(apis.StubPlugin3);
  });
  
  var plugin4io;
  var plugin4log = [];
  
  it('initializes plugin4', function () {
    plugin4io = new PluginIO(
      StubPlugin4,
      data.plugin4.privateKey,
      {}, // dependencies
      {   // locals
        log: plugin4log
      },
      {   // events
        'hello': function (eventName, message) {
          this.log.push([eventName, message]);
        }
      }
    );
    
    assert(plugin4io);
  });
  
  it('authorizes plugin4', function () {
    stackIo.authorizePlugin(
      'StubPlugin4', 
      data.plugin4.publicKey
    );
  });
  
  it('connectes plugin4', function (done) {
    var bus = new EmittersBus();
    
    stackIo
    .listenSocket(bus.createEmitter());
    plugin4io
    .listenSocket(
      bus.createEmitter()
    )
    .then(function () {
      done();
    });
  });
  
  it('plugin4 is listed', function () {
    var apis = stack.listPlugins();
    
    assert(apis.StubPlugin4);
  });
  
  it('hello has subscribers', function () {
    assert(stack.listSubscribers('hello').length);
  });
  
  var client2;
  
  it('client2 connects', function (done) {
    var bus = new EmittersBus();
    
    stackIo
    .listenSocket(bus.createEmitter());
    
    client1io
    .listenSocket(bus.createEmitter(), token1)
    .then(function (client) {
      client2 = client;
      
      try {
        assert(client2);
        assert(client2.StubPlugin2);
        assert(client2.StubPlugin2.whoAmI);
        
        done();
      } catch (e) {
        done(e);
      }
    });
  });
  
  it('client2 uses token', function (done) {
    client2
    .StubPlugin2
    .whoAmI()
    .then(function (username) {
      try {
        assert.equal('user1', username);
        
        done();
      } catch (e) {
        done(e);
      }
    });
  });
  
  it('plugin3 has emit', function (done) {
    client2
    .StubPlugin3
    .hasEmit()
    .then(function (hasEmit) {
      try {
        assert.equal(true, hasEmit);
        
        done();
      } catch (e) {
        done(e);
      }
    });
  });
  
  it('plugin3 emits hello', function (done) {
    var subscriber = new EventEmitter();
    
    stack.addSubscriber(subscriber, 'hello');
    
    subscriber
    .once('event', function (eventName, stackToken, context, message) {
      done();
    });
    
    client2
    .StubPlugin3
    .emitHello();
  });
  
  it('plugin4 received event', function () {
    assert.deepEqual(
      plugin4log,
      [
        [
          'hello',
          {
            message: 'there'
          }
        ]
      ]
    );
  });
  
  var pubsub1io;
  
  it('initializes pubsub1', function () {
    pubsub1io = new PubSubIO(
      data.pubsub1.privateKey
    );
    
    assert(pubsub1io);
  });
  
  var pubsub1;

  it('pubsub1 connects', function (done) {
    var bus = new EmittersBus();
    
    stackIo
    .listenSocket(bus.createEmitter());
    
    var socket1 = bus.createEmitter();
    
    pubsub1io
    .useSocket(socket1)
    .then(function (pubsub) {
      try {
        assert(pubsub);
        
        pubsub1 = pubsub;

        done();
      } catch (e) {
        done(e);
      }
    });
  });
});
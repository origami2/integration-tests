function StubPlugin1() {
}

StubPlugin1.prototype.echo = function (what) {
  return Promise.resolve(what);
};

module.exports = StubPlugin1;